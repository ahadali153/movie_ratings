from django.contrib import admin
from movies.models import Movies
# Register your models here.

@admin.register(Movies)
class MoviesAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "picture",
        "synopsis",
        "released",
    )
