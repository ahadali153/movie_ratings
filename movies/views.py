from django.shortcuts import render, redirect
from movies.models import Movies
from movies.forms import MoviesForm

# Create your views here.
def show_movies(request):
    movie_list = Movies.objects.all()
    context = {
        "movie_list": movie_list
    }
    return render(request, 'movies/home.html', context)

def add_movie(request):
    if request.method == "POST":
        form = MoviesForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            if Movies.objects.filter(name=name).exists():
                pass
            else:
                form.save()
                return redirect("home")
    else:
        form = MoviesForm()
    context = {"form": form}
    return render(request, 'movies/create.html', context)
