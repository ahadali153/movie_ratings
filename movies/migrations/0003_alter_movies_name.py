# Generated by Django 4.2 on 2023-04-27 17:47

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("movies", "0002_rename_movieslist_movies"),
    ]

    operations = [
        migrations.AlterField(
            model_name="movies",
            name="name",
            field=models.CharField(max_length=100, unique=True),
        ),
    ]
