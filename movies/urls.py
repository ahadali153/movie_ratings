from django.urls import path
from movies.views import show_movies, add_movie

urlpatterns = [
    path("movies/", show_movies, name="home"),
    path("add/", add_movie, name="add_movie")
]
