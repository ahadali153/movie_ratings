from django.forms import ModelForm
from movies.models import Movies

class MoviesForm(ModelForm):
    class Meta:
        model = Movies
        fields = [
            "name",
            "picture",
            "synopsis",
            "released",
        ]
